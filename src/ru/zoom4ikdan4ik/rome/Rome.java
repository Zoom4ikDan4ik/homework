package ru.zoom4ikdan4ik.rome;

import ru.zoom4ikdan4ik.rome.interfaces.IRule;
import ru.zoom4ikdan4ik.rome.rules.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Rome {
    //Методика действий основано на принципе "палочек".
    //К примеру V = "IIIII"
    //Стандартный main метод при запуске Java приложения, ссылка на него в META-INF
    //Можно запустить с имеющимися аргументами по типу java -jar Rome.jar XXL + LV
    //Либо при обычном запуске по типу java -jar Rome.jar, в дальнейшем вводить на основе "1-е число" "действие" "2-е число" "n-действие" "n-е число"
    public static void main(String[] args) throws IOException {
        //В случае, если аргументов нет, записываем данные через BufferedReader
        if (args.length == 0) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

            List<String> list = new ArrayList<>();
            String line;

            //Если строка пустая, то считается, что больше иных аргументов нет
            while (!(line = bufferedReader.readLine()).isEmpty())
                list.add(line);

            args = new String[list.size()];

            //Присваиваем пустому массиву аргументов полученные данные черех BufferedReader
            for (int index = 0; index < list.size(); index++)
                args[index] = list.get(index);
        }

        //Приводим все значения к транслиту для дальейшего видоизменения и действий
        for (int index = 0; index < args.length; index++)
            args[index] = args[index].toUpperCase();

        //Получаем 1-е число
        String rome = args[0];

        //Получаем действие между n-числом и n+1 числом, избегая сами значения
        for (int index = 1; index < args.length; index += 2) {
            IRule rule;

            String mode = args[index];

            //Реализация дальнейших действий
            //Получаем класс с реализацией
            //С помощью интерфейса IRule используем описанный в нем метод для его вывода в консоль
            //В каждом классе Rome"действие"Rule своя методика вычисления между имеющимся результатом и дальнейшим n-числом
            switch (mode) {
                case "-":
                    rule = new RomeSubtractionRule(rome, args[index + 1]);

                    rome = rule.printResult();

                    break;
                case "+":
                    rule = new RomeSummationRule(rome, args[index + 1]);

                    rome = rule.printResult();

                    break;
                case "*":
                    rule = new RomeMultiplicationRule(rome, args[index + 1]);

                    rome = rule.printResult();

                    break;
                case "/":
                    rule = new RomeDivisionRule(rome, args[index + 1]);

                    rome = rule.printResult();

                    break;
                case ":":
                    rule = new RomeDivisionRule(rome, args[index + 1]);

                    rome = rule.printResult();

                    break;
                case "^":
                    rule = new RomeDegreeRule(rome, args[index + 1]);

                    rome = rule.printResult();

                    break;
            }
        }
    }
}
