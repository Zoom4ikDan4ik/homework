package ru.zoom4ikdan4ik.rome.rules;

import ru.zoom4ikdan4ik.rome.rules.base.RomeBase;

//Метод возведения в степень
public class RomeDegreeRule extends RomeBase {
    private String first;
    private String second;

    public RomeDegreeRule(String first, String second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public final String printResult() {
        this.setResult(this.getFullSize(this.first));

        for (int i = 1; i < this.getFullSize(this.second).length(); i++) {
            String module = this.getResult();

            for (int j = 1; j < this.getFullSize(this.first).length(); j++)
                this.setResult(this.getResult() + module);
        }

        return super.printResult();
    }
}
