package ru.zoom4ikdan4ik.rome.rules;

import ru.zoom4ikdan4ik.rome.rules.base.RomeBase;

//Метод деления 1-го на 2-е
public class RomeDivisionRule extends RomeBase {
    private String first;
    private String second;

    public RomeDivisionRule(String first, String second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public final String printResult() {
        this.setResult("");

        for (int i = this.getFullSize(this.first).length(); i > 0; i -= this.getFullSize(this.second).length())
            this.setResult(this.getResult() + "I");

        return super.printResult();
    }
}
