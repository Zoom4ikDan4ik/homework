package ru.zoom4ikdan4ik.rome.rules;

import ru.zoom4ikdan4ik.rome.rules.base.RomeBase;

//Метод умножения
public class RomeMultiplicationRule extends RomeBase {
    private String first;
    private String second;

    public RomeMultiplicationRule(String first, String second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public final String printResult() {
        this.setResult("");

        //Умножение, путем сложения 1-го числа само на себя столько раз, сколько равно 2-е число
        for (int i = 0; i < this.getFullSize(this.second).length(); i++)
            this.setResult(this.getResult() + this.getFullSize(this.first));

        return super.printResult();
    }
}
