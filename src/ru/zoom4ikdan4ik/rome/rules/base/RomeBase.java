package ru.zoom4ikdan4ik.rome.rules.base;

import ru.zoom4ikdan4ik.rome.interfaces.IRule;

//Базовый класс, в котором идет анализ значения в result
public class RomeBase implements IRule {
    private String result;

    //Получение результата
    protected final String getResult() {
        return this.result;
    }

    //Установка значения
    protected final void setResult(String result) {
        this.result = result;
    }

    //Приведение к сокращенным значениями имеющегося результата
    private final boolean fixing() {
        boolean flag = false;

        //Поиск не сокращенного
        if (this.getResult().contains("DD") || this.getResult().contains("DM") || this.getResult().contains("CCCC") || this.getResult().contains("CCCCC") ||
                this.getResult().contains("LL") || this.getResult().contains("LC") || this.getResult().contains("XXXX") || this.getResult().contains("XXXXX") ||
                this.getResult().contains("VV") || this.getResult().contains("VX") || this.getResult().contains("IIII") || this.getResult().contains("IIIII")
        ) {
            this.setResult(this.getResult()
                    .replaceAll("IIIII", "V")
                    .replaceAll("IIII", "IV")
                    .replaceAll("VV", "X")
                    .replaceAll("VX", "V")
                    .replaceAll("XXXXX", "L")
                    .replaceAll("XXXX", "XL")
                    .replaceAll("LL", "C")
                    .replaceAll("LC", "L")
                    .replaceAll("CCCCC", "D")
                    .replaceAll("CCCC", "CD")
                    .replaceAll("DD", "M")
                    .replaceAll("DM", "D")
            );

            flag = true;
        }

        return flag;
    }

    //Получение полного (несокращенного) значения
    protected final String getFullSize(String string) {
        boolean flag = true;

        //Заменяем все сокращения до тех пор, пока все не бует сокращено
        while (flag) {
            string = string
                    .replaceAll("M", "DD")
                    .replaceAll("CD", "CCCC")
                    .replaceAll("D", "CCCCC")
                    .replaceAll("C", "LL")
                    .replaceAll("XL", "XXXX")
                    .replaceAll("L", "XXXXX")
                    .replaceAll("X", "VV")
                    .replaceAll("IV", "IIII")
                    .replaceAll("V", "IIIII");

            flag = string.contains("M") || string.contains("D") || string.contains("C") || string.contains("L") || string.contains("X") || string.contains("V");
        }

        return string;
    }

    public final void print() {
        System.out.println("= " + this.getResult());
    }

    @Override
    public String printResult() {
        //Опасная строка без какой-либо специальной реализации в теле while, но работающая
        while (this.fixing()) ;

        //Вывод результата
        this.print();

        //Возврат результата
        return this.result;
    }
}
