package ru.zoom4ikdan4ik.rome.rules;

import ru.zoom4ikdan4ik.rome.rules.base.RomeBase;

public class RomeSummationRule extends RomeBase {
    private String first;
    private String second;

    public RomeSummationRule(String first, String second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public final String printResult() {
        //Просто сложение 2-х значений
        this.setResult(this.getFullSize(this.first) + this.getFullSize(this.second));

        return super.printResult();
    }
}
