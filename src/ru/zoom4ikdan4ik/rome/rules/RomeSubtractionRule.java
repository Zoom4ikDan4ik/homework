package ru.zoom4ikdan4ik.rome.rules;

import ru.zoom4ikdan4ik.rome.rules.base.RomeBase;

//Метод вычитания
public class RomeSubtractionRule extends RomeBase {
    private String first;
    private String second;

    public RomeSubtractionRule(String first, String second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public final String printResult() {
        //Вычитаем значение из первого через replaceFirst
        this.setResult(this.getFullSize(this.first).replaceFirst(this.getFullSize(this.second), ""));

        return super.printResult();
    }
}
