package ru.zoom4ikdan4ik.turing.streams;

import ru.zoom4ikdan4ik.turing.machine.MachineLogic;
import ru.zoom4ikdan4ik.turing.machine.MachineMarker;
import ru.zoom4ikdan4ik.turing.machine.MachineReplace;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileReaderStream {
    private final File file;

    public FileReaderStream() {
        this.file = new File("input");

        if (!this.file.exists()) {
            try {
                this.file.createNewFile();

                this.writeDefaultInfo();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public final List<String> getLines() {
        List<String> lines = new ArrayList<>();

        try (FileReader fileReader = new FileReader(this.file)) {
            Scanner scanner = new Scanner(fileReader);

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();

                if (!line.isEmpty() && !line.contains("#"))
                    lines.add(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return lines;
    }

    private void writeDefaultInfo() {
        FileWriterStream fileWriterStream = new FileWriterStream(this.file.getName());
        List<String> info = new ArrayList<>();

        info.add("#Интерпритатор машины Тьюринга");
        info.add("#Структура: 'заменяемое (любое значение с символами ' ', '*')=>'замена';'указатель маркера (>,<,->,<-)';'следующее состояние'");
        info.add("#Разработал Залов Д.В.");

        fileWriterStream.writeLines(info);
    }

    public List<MachineLogic> getMachineLogics() {
        List<MachineLogic> machineLogics = new ArrayList<>();
        List<String> lines = this.getLines();

        for (String line : lines) {
            String[] splitter = line.split(";");
            String[] splitterReplace = splitter[0].split("=>");

            MachineReplace replace = null;
            if (splitterReplace.length != 0)
                replace = new MachineReplace(splitterReplace[0].charAt(0), splitterReplace[1].charAt(0));

            MachineMarker machineMarker = MachineMarker.getMarker(splitter[1]);
            int nextID = Integer.parseInt(splitter[2]);

            machineLogics.add(new MachineLogic(replace, machineMarker, nextID));
        }

        return machineLogics;
    }
}
