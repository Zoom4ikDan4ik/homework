package ru.zoom4ikdan4ik.turing.streams;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class FileWriterStream {
    private final File file;

    public FileWriterStream() {
        this("output");
    }

    public FileWriterStream(String name) {
        this.file = new File(name);

        if (!this.file.exists()) {
            try {
                this.file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void writeLine(String line) {
        try (FileWriter fileWriter = new FileWriter(this.file)) {
            fileWriter.write(line);

            fileWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeLines(List<String> lines) {
        try (FileWriter fileWriter = new FileWriter(this.file)) {
            for (String line : lines)
                fileWriter.write(line + "\n");

            fileWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
