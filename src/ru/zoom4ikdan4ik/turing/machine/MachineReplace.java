package ru.zoom4ikdan4ik.turing.machine;

public class MachineReplace {
    private final char first;
    private final char second;

    public MachineReplace(final char first, final char second) {
        this.first = first;
        this.second = second;
    }

    public final char getFirst() {
        return this.first;
    }

    public final char getSecond() {
        return this.second;
    }

    public final boolean contains(char ch) {
        return this.getFirst() == ' ' || this.getFirst() == '*' || this.getFirst() == ch;
    }
}
