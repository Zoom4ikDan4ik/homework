package ru.zoom4ikdan4ik.turing.machine;

import java.util.ArrayList;
import java.util.List;

public enum MachineMarker {
    RIGHT(">", "->"),
    LEFT("<", "<-");

    private List<String> associations = new ArrayList<>();

    MachineMarker(String... strings) {
        for (String string : strings)
            this.associations.add(string);
    }

    public static final MachineMarker getMarker(String string) {
        for (MachineMarker machineMarker : values())
            for (String assoc : machineMarker.getAssociations())
                if (assoc.equalsIgnoreCase(string))
                    return machineMarker;

        return null;
    }

    public final List<String> getAssociations() {
        return this.associations;
    }
}
