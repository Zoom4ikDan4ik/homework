package ru.zoom4ikdan4ik.turing.machine;

public class MachineLogic {
    private final MachineReplace replace;
    private final MachineMarker machineMarker;
    private final int nextID;

    public MachineLogic(final MachineReplace replace, final MachineMarker machineMarker, final int nextID) {
        this.replace = replace;
        this.machineMarker = machineMarker;
        this.nextID = nextID;
    }

    public final MachineReplace getReplace() {
        return this.replace;
    }

    public final MachineMarker getMachineMarker() {
        return this.machineMarker;
    }

    public final int getNextID() {
        return this.nextID;
    }
}
