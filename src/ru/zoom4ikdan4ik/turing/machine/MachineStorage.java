package ru.zoom4ikdan4ik.turing.machine;

import java.util.LinkedList;

public class MachineStorage {
    private final LinkedList<MachineLogic> machineLogics = new LinkedList<>();

    public final void addMachineLogic(final int index, final MachineLogic machineLogic) {
        this.machineLogics.add(index, machineLogic);
    }

    public final MachineLogic getMachineLogic(final int index) {
        return this.machineLogics.get(index);
    }
}
