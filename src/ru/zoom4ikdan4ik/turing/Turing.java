package ru.zoom4ikdan4ik.turing;

import ru.zoom4ikdan4ik.turing.machine.MachineLogic;
import ru.zoom4ikdan4ik.turing.machine.MachineMarker;
import ru.zoom4ikdan4ik.turing.machine.MachineReplace;
import ru.zoom4ikdan4ik.turing.machine.MachineStorage;
import ru.zoom4ikdan4ik.turing.streams.FileReaderStream;
import ru.zoom4ikdan4ik.turing.streams.FileWriterStream;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class Turing {
    private final static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    private final static FileReaderStream fileReaderStream = new FileReaderStream();
    private final static FileWriterStream fileWriterStream = new FileWriterStream();

    private final static MachineStorage machineStorage = new MachineStorage();

    public static void main(String[] args) throws IOException {
        List<MachineLogic> machineLogics = fileReaderStream.getMachineLogics();

        for (int index = 0; index < machineLogics.size(); index++)
            machineStorage.addMachineLogic(index, machineLogics.get(index));

        String data = bufferedReader.readLine();

        int id = 0;
        int idChar = 0;

        do {
            MachineLogic machineLogic = machineStorage.getMachineLogic(id);

            MachineReplace replace = machineLogic.getReplace();
            MachineMarker machineMarker = machineLogic.getMachineMarker();

            char[] chars = data.toCharArray();

            if (replace != null)
                if (replace.contains(chars[idChar]))
                    chars[idChar] = replace.getSecond();

            if (machineMarker == MachineMarker.LEFT)
                idChar -= 1;
            else if (machineMarker == MachineMarker.RIGHT)
                idChar += 1;

            id = machineLogic.getNextID();
            data = String.valueOf(chars);

            System.out.println(data);

            fileWriterStream.writeLine(data);
        } while (id != 0 && idChar < data.length());
    }
}
